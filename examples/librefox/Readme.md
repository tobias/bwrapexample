# Librefox Example

This is a generic firefox under fedora example, but with user config for librefox overrides.
Sadly bwrap enforces files that it overrides to already exist, but you can just add empty files to mozilla.cfg and co for example.

## This example assumes the "sandbox" folder is under $YOURHOME/sandbox/firefox.sh/

And you have to replace all placeholders in addconf.sh (even $HOME) with the full path.
Make sure to also replace the "USERNAMEHERE" folder in main/home.
And because of the nature of bwrap only overriding existing files add (or touch) all files mentioned in the addconf.sh.

Example:

``` bash
sudo -i
mkdir -p /usr/lib64/firefox/defaults/pref
mkdir -p /usr/lib64/firefox/defaults/distribution
touch /usr/lib64/firefox/mozilla.cfg
touch /usr/lib64/firefox/defaults/pref/local-settings.js
touch /usr/lib64/firefox/defaults/distribution/policies.json
exit
```

## All fine but what does this do? Well

This will get you a sandboxed firefox, default-configured for privacy, that you can update by just moving the new policy files from [the Librefox Repo](https://github.com/intika/Librefox/tree/master/librefox) into addff