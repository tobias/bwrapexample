# bwrapexample

This is a tiny example for how to use [bubblewrap](https://github.com/projectatomic/bubblewrap) to isolate programs into namespaced everything (except network access)

Keep in mind, this is NOT a 100% breakout-resistant thing. However it could be used to protect processes from reading files they are not supposed to see, and have them locked "good enough".

Userdata will be persisted so you can run a separate browser instance as well. Or create a startup script inside the new home folder of the sandbox and run it via sandbox.sh ./start.sh.

## Usage

Calling just a tool:

``` bash
./sandbox.sh bash
```

Running an entire script in the sandbox, giving you way more code options:

``` bash
./sandbox.sh ./firefox.sh
```

(This would call ~/sandbox.sh/main/home/USERNAMEHERE/firefox.sh)

## KNOWN PROBLEMS (Examples)

### vscodium

Some tools, like vscodium, use a launcher that dies when the program starts.
With the script dying if the first spawned child process dies this will lead
to these tools not running in this particular config.
You will need to remove:

``` bash
--die-with-parent
--as-pid-1
```

from the Script to make it work, at the risk of tools not closing when the console/program closes.

Or you could override these in a user-given addconf.sh file, see examples in the example/librefox folder

### steam

Steam will die if you use the "Save Password" Option, because dbus fails.

I get this could be pretty annoying to use, so instead you can:

edit the steam.desktop file

replace "Exec" with "Exec=/path/to/sandbox.sh ./steam.sh"

And then create the file: $HOME/steam.sh/main/home/$YOUR_USERNAME_HERE/steam.sh
with the contents:

``` bash
steam -login $YOURUSERNAME $PASSWORD
```

or

``` bash
steam -login '$YOURUSERNAME' '$PASSWORD'
```

(Make sure the args get escaped well if you have complex username/pw combos)

Beware, this will expose your credentials to all processes that can read your other system processes, but it does make steam login flawless and avoids the crash.